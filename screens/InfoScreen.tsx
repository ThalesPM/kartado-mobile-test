import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Header from '../components/Header';
import TextInput from '../components/TextInput';
import {theme} from '../styles/theme';
import Button from '../components/Button';
import request from '../api/call';

const InfoScreen = () => {
  const [info, setInfo] = useState({value: '', error: ''});

  const apiCall = async () => {
    const {success, data, error} = await request();
    console.log(success, data, error);
  };

  const _onSendPressed = () => {
    console.log('Send to API');
    apiCall();
  };

  return (
    <View style={styles.view}>
      <Header>Testar API</Header>

      <TextInput
        accessibilityStates
        label="Texto "
        returnKeyType="done"
        value={info.value}
        onChangeText={(text) => setInfo({value: text, error: ''})}
        error={!!info.error}
        errorText={info.error}
        autoCapitalize="none"
      />

      <Button
        accessibilityStates
        mode="contained"
        onPress={_onSendPressed}
        style={styles.button}>
        Enviar API
      </Button>

      <Header>Resposta:</Header>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  view: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  back: {
    marginTop: 12,
  },
  button: {
    marginTop: 12,
  },
  label: {
    color: theme.colors.secondary,
    width: '100%',
  },
});

export default InfoScreen;
