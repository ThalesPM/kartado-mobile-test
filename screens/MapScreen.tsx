import * as React from 'react';
import MapboxGL from '@react-native-mapbox-gl/maps';
import sheet from '../styles/sheet';

export default function MapScreen() {
  return (
    <MapboxGL.MapView style={sheet.matchParent}>
      <MapboxGL.Camera followZoomLevel={12} followUserLocation />
      <MapboxGL.UserLocation />
    </MapboxGL.MapView>
  );
}
