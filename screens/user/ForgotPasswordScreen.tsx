import React, {memo, useState} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {emailValidator} from '../../utils';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import TextInput from '../../components/TextInput';
import {theme} from '../../styles/theme';
import Button from '../../components/Button';
import {Navigation} from '../../types';

type Props = {
  navigation: Navigation;
};

const ForgotPasswordScreen = ({navigation}: Props) => {
  const [email, setEmail] = useState({value: '', error: ''});

  const _onSendPressed = () => {
    const emailError = emailValidator(email.value);

    if (emailError) {
      setEmail({...email, error: emailError});
      return;
    }

    navigation.navigate('login');
  };

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.view}>
        <Logo />

        <Header>Recuperar sua senha</Header>

        <TextInput
          accessibilityStates
          label="Endereço de Email"
          returnKeyType="done"
          value={email.value}
          onChangeText={(text) => setEmail({value: text, error: ''})}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />

        <Button
          accessibilityStates
          mode="contained"
          onPress={_onSendPressed}
          style={styles.button}>
          Enviar instruções
        </Button>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  view: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  back: {
    marginTop: 12,
  },
  button: {
    marginTop: 12,
  },
  label: {
    color: theme.colors.secondary,
    width: '100%',
  },
});

export default memo(ForgotPasswordScreen);
