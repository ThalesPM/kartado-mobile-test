import React, {memo} from 'react';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import Paragraph from '../../components/Paragraph';
import Button from '../../components/Button';
import {Navigation} from '../../types';
import {StyleSheet, View} from 'react-native';

type Props = {
  navigation: Navigation;
};

const Dashboard = ({navigation}: Props) => (
  <View style={styles.view}>
    <Logo />
    <Header>Let’s start</Header>
    <Paragraph>
      Your amazing app starts here. Open you favourite code editor and start
      editing this project.
    </Paragraph>
    <Button
      accessibilityStates
      mode="outlined"
      onPress={() => navigation.navigate('dashboard')}>
      Logout
    </Button>
  </View>
);

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default memo(Dashboard);
