import React, {memo, useState} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import {theme} from '../../styles/theme';
import {emailValidator, passwordValidator} from '../../utils';
import {Navigation} from '../../types';

type Props = {
  navigation: Navigation;
};

const LoginScreen = ({navigation}: Props) => {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});

  const _onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }

    navigation.navigate('Dashboard');
  };

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.view}>
        <Logo />

        <Header>Bem-Vindo</Header>

        <TextInput
          accessibilityStates
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({value: text, error: ''})}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />

        <TextInput
          accessibilityStates
          label="Senha"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({value: text, error: ''})}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate('forgotPassword')}>
            <Text style={styles.label}>Esqueceu sua senha?</Text>
          </TouchableOpacity>
        </View>

        <Button accessibilityStates mode="contained" onPress={_onLoginPressed}>
          Entrar
        </Button>

        <View style={styles.row}>
          <Text style={styles.label}>Você não possui uma conta? </Text>
          <TouchableOpacity onPress={() => navigation.navigate('register')}>
            <Text style={styles.link}>Registrar-se</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  forgotPassword: {
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
    paddingBottom: 10,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  scrollView: {
    backgroundColor: 'white',
  },
  view: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default memo(LoginScreen);
