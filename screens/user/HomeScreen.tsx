import React, {memo} from 'react';
import Logo from '../../components/Logo';
import Button from '../../components/Button';
import Paragraph from '../../components/Paragraph';
import {Navigation} from '../../types';
import {StyleSheet, View} from 'react-native';

type Props = {
  navigation: Navigation;
};

const HomeScreen = ({navigation}: Props) => (
  <View style={styles.view}>
    <Logo />
    <Paragraph>Acesse sua conta para explorar ao máximo</Paragraph>
    <Button
      accessibilityStates
      mode="contained"
      onPress={() => navigation.navigate('login')}>
      Entrar
    </Button>
    <Button
      accessibilityStates
      mode="outlined"
      onPress={() => navigation.navigate('register')}>
      Registrar nova conta
    </Button>
  </View>
);

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default memo(HomeScreen);
