export default async function call(): Promise<{
  success: boolean;
  error: string;
  data: any;
}> {
  try {
    const response = await fetch('/api');
    if (response.status === 401) {
      return {
        success: false,
        error: '',
        data: null,
      };
    } else {
      const data = await response.json();
      return {
        success: true,
        error: '',
        data,
      };
    }
  } catch (e) {
    console.error(e);
    return {
      success: false,
      error: 'api-error',
      data: null,
    };
  }
}
