import 'react-native-gesture-handler';
import React, {useState, useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Navigation from './navigation';
import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {IS_ANDROID} from './utils';
import sheet from './styles/sheet';
import colors from './styles/colors';
import {theme} from './styles/theme';

const styles = StyleSheet.create({
  noPermissionsText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});

MapboxGL.setAccessToken(
  'pk.eyJ1IjoidGhhbGVzcG0iLCJhIjoiY2tjc2F1eXRwMWRrNzJ5b2J1c3M1eHlieCJ9.SmnnTiMDfdXyFG4E8L-8cw',
);

export default function App() {
  const [isAndroidPermissionGranted, setIsAndroidPermissionGranted] = useState(
    false,
  );
  const [
    isFetchingAndroidPermission,
    setIsFetchingAndroidPermission,
  ] = useState(IS_ANDROID);

  useEffect(() => {
    if (IS_ANDROID) {
      async function loadPermissions() {
        const isGranted = await MapboxGL.requestAndroidLocationPermissions();
        setIsAndroidPermissionGranted(isGranted);
        setIsFetchingAndroidPermission(false);
      }

      loadPermissions();
    }
  }, []);

  if (IS_ANDROID && !isAndroidPermissionGranted) {
    if (isFetchingAndroidPermission) {
      return null;
    }
    return (
      <SafeAreaView
        style={[sheet.matchParent, {backgroundColor: colors.primary.blue}]}>
        <View style={sheet.matchParent}>
          <Text style={styles.noPermissionsText}>
            You need to accept location permissions in order to use this example
            applications
          </Text>
        </View>
      </SafeAreaView>
    );
  }
  return (
    <SafeAreaProvider>
      <PaperProvider theme={theme}>
        <Navigation />
      </PaperProvider>
    </SafeAreaProvider>
  );
}
