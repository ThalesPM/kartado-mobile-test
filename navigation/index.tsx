import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {Menu} from 'react-native-paper';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MapScreen from '../screens/MapScreen';
import InfoScreen from '../screens/InfoScreen';
import {
  HomeScreen,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  Dashboard,
} from '../screens/user';

import {theme} from '../styles/theme';

const Tab = createBottomTabNavigator();
const MapStack = createStackNavigator();
const SettingStack = createStackNavigator();
const UserStack = createStackNavigator();

function MapStackScreen() {
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  return (
    <MapStack.Navigator
      screenOptions={() => ({
        headerTintColor: theme.colors.primary,
        headerTitleStyle: {
          alignSelf: 'center',
        },
        headerRight: () => (
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
              <Ionicons
                name="settings-outline"
                size={20}
                color={theme.colors.primary}
                style={{marginRight: 10}}
                onPress={openMenu}
              />
            }>
            <Menu.Item onPress={() => {}} title="Configurações" />
            <Menu.Item onPress={() => {}} title="Suporte" />
          </Menu>
        ),
      })}>
      <MapStack.Screen
        name="map"
        component={MapScreen}
        options={{title: 'Mapa'}}
      />
    </MapStack.Navigator>
  );
}

function InfoStackScreen() {
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  return (
    <SettingStack.Navigator
      screenOptions={() => ({
        headerTintColor: theme.colors.primary,
        headerTitleStyle: {
          alignSelf: 'center',
        },
        headerRight: () => (
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
              <Ionicons
                name="settings-outline"
                size={20}
                color={theme.colors.primary}
                style={{marginRight: 10}}
                onPress={openMenu}
              />
            }>
            <Menu.Item onPress={() => {}} title="Configurações" />
            <Menu.Item onPress={() => {}} title="Suporte" />
          </Menu>
        ),
      })}>
      <SettingStack.Screen
        name="info"
        component={InfoScreen}
        options={{title: 'Informações'}}
      />
    </SettingStack.Navigator>
  );
}

function UserStackScreen() {
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  return (
    <UserStack.Navigator
      initialRouteName="user"
      screenOptions={() => ({
        headerTintColor: theme.colors.primary,
        headerTitleStyle: {
          alignSelf: 'center',
        },
        headerRight: () => (
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            anchor={
              <Ionicons
                name="settings-outline"
                size={20}
                color={theme.colors.primary}
                style={{marginRight: 10}}
                onPress={openMenu}
              />
            }>
            <Menu.Item onPress={() => {}} title="Configurações" />
            <Menu.Item onPress={() => {}} title="Suporte" />
          </Menu>
        ),
      })}>
      <UserStack.Screen
        name="user"
        component={HomeScreen}
        options={{title: 'Acesso'}}
      />
      <UserStack.Screen
        name="login"
        component={LoginScreen}
        options={{title: 'Entrar'}}
      />
      <UserStack.Screen
        name="register"
        component={RegisterScreen}
        options={{title: 'Registrar'}}
      />
      <UserStack.Screen
        name="forgotPassword"
        component={ForgotPasswordScreen}
        options={{title: 'Redefinir Senha'}}
      />
      <UserStack.Screen
        name="dashboard"
        component={Dashboard}
        options={{title: 'Painel'}}
      />
    </UserStack.Navigator>
  );
}

export default function Navigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="map-tab"
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName = '';
            if (route.name === 'info-tab') {
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
            } else if (route.name === 'map-tab') {
              iconName = focused ? 'map' : 'map-outline';
            } else if (route.name === 'user-tab') {
              iconName = focused ? 'person-circle' : 'person-circle-outline';
            }
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          headerTintColor: theme.colors.primary,
          headerTitleStyle: {
            alignSelf: 'center',
          },
        })}
        tabBarOptions={{
          activeTintColor: theme.colors.primary,
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen
          name="info-tab"
          options={{title: 'Informações'}}
          component={InfoStackScreen}
        />
        <Tab.Screen
          name="map-tab"
          options={{title: 'Mapa'}}
          component={MapStackScreen}
        />
        <Tab.Screen
          name="user-tab"
          options={{title: 'Usuário'}}
          component={UserStackScreen}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
